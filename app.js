const Application = require('./src/Application');

const config = require('config');

const application = new Application(config.get('Application'));

const startApplications = async () => {
    application.bootstrap();

    await application.start();
};

const onStarted = () => {
    console.log(`${application.constructor.name} starts on ${application.host}:${application.port}`);
};

const stopApplications = (emergency = false) => {
    application.stop();

    process.exit(emergency ? 1 : 0);
};

const onError = (e) => {
    console.log(e);

    stopApplications(true);
};

process.on('SIGTERM', stopApplications);
process.on('uncaughtException', onError);

startApplications()
    .then(onStarted)
    .catch((e) => {
        console.log(e);

        stopApplications(true);
    });