const fillWithZero = (l) => {
    const result = [];

    for (let i = 0; i < l; i++) {
        const temp = [];

        for (let j = 0; j < l; j++) {
            temp.push(0);
        }

        result.push(temp);
    }

    return result;
};

class DataProcessor {

    static convertToCoordinates(data) {
        return data.split('\n').map(item => {
            const [x, y] = item.split(',');
            return {x, y};
        });
    }

    /**
     * used
     */
    static getMovementDirection(x2, y2, direction) {
        //scalar production of vectors
        const scalar = Math.pow(x2, 2);

        //|Vector A|
        const mA = Math.sqrt((Math.pow(x2, 2) + Math.pow(x2, 2)));
        const mB = Math.sqrt((Math.pow(0, 2) + Math.pow(y2, 2)));

        const radian = Math.acos(scalar / (mA * mB));
        const degrees = radian * 180 / Math.PI;

        if (direction) {
            if (y2 > 0) {
                return degrees > 45 ? {u: 1} : {r: 1};
            } else {
                return degrees > 45 ? {d: 1} : {r: 1};
            }
        } else {
            if (y2 > 0) {
                return degrees > 45 ? {u: 1} : {l: 1};
            } else {
                return degrees > 45 ? {d: 1} : {l: 1};
            }
        }
    }

    static getVectorMatrix(direction, y2, x2, plot) {
        // 'cause we use array of arrays for chart representation
        const coefficient = 20;

        if (direction) {
            for (let x = 20; x < 40; x++) {
                const y = Math.floor((((x - 20) * +y2) / +x2));

                if ((y + coefficient) < 0 || (y + coefficient) > 39) continue;

                plot[(y + coefficient)][x] = 1;
            }
        } else {
            for (let x = 20; x > 0; x--) {
                const y = Math.floor((((x - 20) * +y2) / +x2));

                if ((y + coefficient) < 0 || (y + coefficient) > 39) continue;

                plot[(y + coefficient)][x] = 1;
            }
        }
    }

    static convertToVector(data, onlyMatrix) {
        const {x: x2, y: y2} = data;

        const direction = +x2 > 0;

        const plot = fillWithZero(40);

        DataProcessor.getVectorMatrix(direction, y2, x2, plot);

        const result = String(plot.reverse() + []).split(',');

        return onlyMatrix ? result : {input: result, output: DataProcessor.getMovementDirection(x2, y2, direction)};
    }

    static prepareDataForMLFromStream(fileStream) {
        return new Promise((resolve) => {
            const temp = [];

            fileStream.on('data', (data) => {
                temp.push(data.toString());
            });

            fileStream.on('end', () => {
                resolve(
                    DataProcessor
                        .convertToCoordinates(temp.join(''))
                        .map(item => {
                            return DataProcessor.convertToVector(item, false);
                        })
                );
            });

        });
    }
}

module.exports = DataProcessor;