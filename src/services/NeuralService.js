const {NeuralNetwork} = require('brain.js');

class NeuralService {
    constructor() {
        /** @type {null|NeuralNetwork}*/
        this._network = null;
    }

    bootstrap() {

    }

    start() {
        this._network = new NeuralNetwork();
    }

    train(train_data) {
        train_data.forEach(data => {
            this._network.train({
                input: data.slice(1, data.length - 1),
                output: data[0]
            });
        });
    }

    stop() {

    }
}

module.exports = NeuralService;