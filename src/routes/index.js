const requireAll = require('require-all');

const routes = requireAll({
    dirname: __dirname,
    filter: /(.+Router)\.js$/,
    excludeDirs: /^\.(git|svn)$/,
    recursive: true
});
/**
 *
 * @param {fastify} fastify
 * @param {Application} application
 */
module.exports.applyRoutes = (fastify, application) => {
    Object.values(routes).map(route => {
        route(fastify, application);
    });
};