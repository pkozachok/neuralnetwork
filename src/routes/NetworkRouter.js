const DataProcessor = require('../services/DataProcessor');

module.exports = (fastify, app) => {
    const neuralNetwork = app.neuralNetwork;

    fastify.post('/network/train', async (req, res) => {
        const handler = async (field, file, filename, encoding, mimetype) => {
            if (mimetype !== 'text/csv') return res.status(500).send();

            const mlData = await DataProcessor.prepareDataForMLFromStream(file);

            console.time('NN train time');
            const result = await neuralNetwork.trainAsync(mlData, {log: true, errorThresh: 0.0003});
            console.timeEnd('NN train time');

            return res.send(result);
        };

        req.multipart(handler, (e) => {
            if (e) res.send(e);
        });
    });

    fastify.post('/network/run', async (req, res) => {
        console.time('NN run time');
        const handler = async (field, file, filename, encoding, mimetype) => {
            if (mimetype !== 'text/csv') return res.status(500).send();

            const temp = [];

            file.on('data', (data) => {
                temp.push(data.toString());
            });

            file.on('end', () => {
                const coordinates = {x: 0, y: 0};

                DataProcessor
                    .convertToCoordinates(temp.join(''))
                    .forEach(coordinate => {
                        coordinates.x += +coordinate.x;
                        coordinates.y += +coordinate.y;
                    });

                const vector = DataProcessor.convertToVector(coordinates, false);

                const result = neuralNetwork.run(vector.input);
                console.timeEnd('NN run time');

                // 'cause g-sensor returns axis load,
                // so x < 0 => up
                // so y < 0 => right
                result.u > 0.9 && res.send({direction: 'down'});
                result.d > 0.9 && res.send({direction: 'up'});
                result.l > 0.9 && res.send({direction: 'right'});
                result.r > 0.9 && res.send({direction: 'left'});
            });
        };

        req.multipart(handler, (e) => {
            if (e) res.send(e);
        });
    });
};