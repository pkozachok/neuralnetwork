const fastify = require('fastify');

const path = require('path');
const fs = require('fs');

const {NeuralNetwork} = require('brain.js');

const routes = require('./routes');

class Application {
    constructor(config) {
        this._config = config;

        this._server = null;

        this._host = this._config.server.host;
        this._port = this._config.server.port;

        /** @type {NeuralNetwork} */
        this._neuralNetwork = new NeuralNetwork();

        this._networkFilePath = path.resolve(process.cwd(), 'network_state.json');
    }

    get neuralNetwork() {
        return this._neuralNetwork;
    }

    get port() {
        return this._port;
    }

    get host() {
        return this._host;
    }

    bootstrap() {
        this._server = fastify();

        routes.applyRoutes(this._server, this);

        this._server.register(require('fastify-multipart'));

        if (fs.existsSync(this._networkFilePath)) {
            console.time('NN recreating time');
            const data = fs.readFileSync(this._networkFilePath, 'utf-8');

            this._neuralNetwork.fromJSON(JSON.parse(data));
            console.timeEnd('NN recreating time');
        }
    }

    async start() {
        await this._server.listen(this._port, this._host);
    }

    stop() {
        console.log('Saving NN state');
        const saveNetState = this._neuralNetwork.toJSON();

        fs.writeFileSync(this._networkFilePath, JSON.stringify(saveNetState), 'utf-8');
        console.log(`Saved to ${this._networkFilePath}`);
    }
}

module.exports = Application;